<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('training_id');
            $table->string('location');

            $table->unsignedBigInteger('trainer_id')->nullable();
            $table->string('trainer')->nullable();

            $table->date('date')->nullable();

            $table->char('day')->nullable();
            $table->time('open_time')->nullable();
            $table->time('close_time')->nullable();

            $table->timestamps();

            $table->foreign('training_id')
                ->references('id')
                ->on('trainings')
                ->onDelete('cascade');

            $table->foreign('trainer_id')
                ->references('id')
                ->on('trainers')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
