<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="yandex-verification" content="6ec22a12ac322d90"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link href="{{mix('css/vendor.css')}}" rel="stylesheet">
    <link href="{{mix('css/app.css')}}" rel="stylesheet">
    @stack('head')
</head>
<body class="{{$bodyClass??''}}">
<nav class="navbar">
    <div class="container text-center">
        <div class="d-flex flex-column flex-md-row align-items-center mb-2">
            <a class="navbar__brand " href="/">
                <img src="{{asset('images/logo-small.svg')}}">
                Evolution Fit
            </a>
            <div class="d-flex flex-column flex-md-row ml-md-auto font-weight-bold">
                <div class="navbar__schedule">
                    <div>Пн-Пт: с 7<sup>00</sup> до 23<sup>00</sup></div>
                    <div>Сб-Вс: с 8<sup>00</sup> до 22<sup>00</sup></div>
                </div>
                <a href="mailto:evolution.fit.18@inbox.ru">evolution.fit.18@inbox.ru</a>
                <div>
                    <a class="pr-0"
                       href="tel:93-55-50">93-55-50</a> / <a class="pl-0 pr-0"
                                                             href="tel:93-55-59">93-55-59</a>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center flex-column flex-md-row">
            <ul class="list-unstyled d-flex flex-column flex-md-row align-items-center mb-0">
                <li><a class="pl-md-0 {{ Request::is('schedule')?'active':''  }}"
                       href="{{route('schedule')}}">Расписание</a></li>
                <li><a class="{{ Request::is('promos')?'active':''  }}"
                       href="{{route('promos')}}">Акции</a></li>
                <li><a class="{{ Request::is('cards')?'active':''  }}"
                       href="{{route('cards')}}">Клубные карты</a></li>
                <li><a class="{{ Request::is('trainers')?'active':''  }}"
                       href="{{route('trainers')}}">Тренеры</a></li>
                <li><a class="{{ Request::is('contacts')?'active':''  }}"
                       href="{{route('contacts')}}">Контакты</a></li>
            </ul>
            <button class="ml-md-auto btn btn_primary" type="button"
                    data-toggle="modal" data-target="#callback-modal">Заказать звонок
            </button>
        </div>
    </div>
</nav>
@yield('content')
@section('footer')
    <footer>
        @include('components.map')
        <div class="footer-inner-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4 mb-3 mb-md-0">
                        <img src="{{asset('images/logo-big.svg')}}">
                    </div>
                    <div class="col-12 col-md-4">
                        <ul class="list-unstyled">
                            <li class="nav-item"><a href="{{route('schedule')}}">Расписание</a></li>
                            <li class="nav-item"><a href="{{route('promos')}}">Акции</a></li>
                            <li class="nav-item"><a href="{{route('cards')}}">Клубные карты</a></li>
                            <li class="nav-item"><a href="{{route('trainers')}}">Тренеры</a></li>
                            <li class="nav-item"><a href="{{route('contacts')}}">Контакты</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-4 font-weight-bolder">
                        <ul class="list-unstyled contacts-group mb-5">
                            <li>
                                <i class="material-icons">
                                    location_on
                                </i>
                                г. Сургут, ул. 30 лет Победы, д. 66
                            </li>
                            <li>
                                <i class="material-icons">
                                    phone
                                </i>
                                <a class="pr-0"
                                   href="tel:93-55-50">93-55-50</a> / <a class="pl-0"
                                                                         href="tel:93-55-59">93-55-59</a>
                            </li>
                            <li>
                                <i class="material-icons">
                                    alternate_email
                                </i>
                                <a href="mailto:evolution.fit.18@inbox.ru">evolution.fit.18@inbox.ru</a>
                            </li>
                        </ul>
                        <button class="btn btn_primary" type="button"
                                data-toggle="modal" data-target="#callback-modal">Перезвоните мне
                        </button>
                        <a class="dev-banner" href="https://1creator.ru">
                            Разработка и продвижение - <img src="/images/1creator.svg"
                                                            alt="1CREATOR создание и сопровождение сайтов">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center map-toggle-on">
            <button type="button" class="btn btn_dark"
                    onclick="document.getElementsByTagName('footer')[0].classList.toggle('show-map')">Переключить карту
            </button>
        </div>
    </footer>
@show
<div class="modal fade" id="callback-modal" tabindex="-1" role="dialog" aria-labelledby="callbackModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="modal-content ajax-form" action="/callback" method="post">
            <div class="ajax-form__form">
                <div class="modal-header">
                    <h5 class="modal-title" id="callbackModalLabel">Заказ обратного звонка</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{csrf_field()}}
                    <div class="input">
                        <label for="callback-modal-name">Имя</label>
                        <input id="callback-modal-name" required name="name" type="text">
                    </div>
                    <div class="input">
                        <label for="callback-modal-phone">Телефон</label>
                        <input id="callback-modal-phone" required name="phone" type="tel">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn_primary">Отправить</button>
                </div>
            </div>
            <div class="ajax-form__conclusion callback-submitted-conclusion">
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="h2">Спасибо!</div>
                <div class="text-primary">Мы скоро вам перезвоним.</div>
            </div>
        </form>
    </div>
</div>
<script src="//code.jivosite.com/widget.js" data-jv-id="9FkcgE2PX6" async></script>
<script src="{{mix('js/app.js')}}"></script>
@stack('scripts')
</body>
</html>
