import Vue from "vue";

import moment from 'moment';
import 'moment/locale/ru';

moment.locale('ru');

Vue.filter('f_percent', function (value) {
    if (!value) return 0;
    return (value * 100).toFixed(0);
});

Vue.filter('f_round', function (value) {
    if (!value) return 0;
    return value.toFixed(0);
});

Vue.filter('f_fullName', function (user) {
    if (!user) return "Не указан";
	
    let name = `${user.firstName || ''} ${user.middleName || ''} ${user.lastName || ''}`;
	
	return name || 'Не заполнено';
});

Vue.filter('f_datetime', function (datetime) {
    return moment.utc(datetime).local().locale('ru').format('LLL');
});