@extends('layouts.main', ['bodyClass'=>'p-index'])

@push('head')
    <title>Акции и скидки</title>
@endpush

@section('content')
    <div class="container mb-4">
        @include('components.breadcrumbs',['items'=> [
        ['name'=>'Главная','url'=>'/'],
        ['name'=>'Акции','url'=>route('promos')],
        ]])
    </div>
    <div class="promos">
        <div class="container">
            <div class="h2 mb-5">Текущие акции</div>
            @foreach($promos as $promo)
                <div class="promo {{!$loop->last?'mb-7':''}}">
                    <div class="row">
                        <div class="col-12 col-md-5 d-flex align-items-center">
                            <img class="promo__image"
                                 src="{{asset($promo->image->url)}}">
                        </div>
                        <div class="col-12 col-md-7 d-flex flex-column justify-content-center align-items-start">
                            <div class="bg-brush">
                                <div class="h3">{{$promo->name}}</div>
                            </div>
                            <div class="mb-4">
                                {!! $promo->description !!}
                            </div>
                            {{--<button class="btn btn_dark">Подать заявку</button>--}}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <div class="py-7">
        @include('components.trial-train')
    </div>
@endsection
