<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Services\Attachment\Models\Attachment;
use App\Utils\RequestedScope;

class Promo extends RequestedScope
{
    protected $fillable = ['name', 'description', 'image_id', 'disabled'];
    protected $able_with = ['image'];

    public function image()
    {
        return $this->belongsTo(Attachment::class, 'image_id');
    }

    public function scopeActive($query)
    {
        return $query->where('disabled', 0);
    }
}
