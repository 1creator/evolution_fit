<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\MediaFile;
use App\Services\Attachment\Services\AttachmentService;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function show(Request $r, MediaFile $file)
    {
        return response(['response' => $file]);
    }

    public function upload(Request $r)
    {
        return app(AttachmentService::class)->store($r->file('file'), $r->get('type'));
    }
}
