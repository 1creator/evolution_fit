<?php

namespace App\Services\Attachment\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class AttachmentResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alt' => $this->alt,
            'type' => $this->type,
            'url' => $this->url,
            'thumbnail' => $this->thumbnail,
            'size' => $this->size,
            'order_column' => $this->order_column,
            'created_at' => $this->created_at,
        ];
    }
}
