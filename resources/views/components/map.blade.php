<div class="map">
    <a class="dg-widget-link"
       href="http://2gis.ru/surgut/firm/70000001034922542/center/73.447734,61.256534/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть
        на карте Сургута</a>
    <div class="dg-widget-link"><a
                href="http://2gis.ru/surgut/center/73.447734,61.256534/zoom/16/routeTab/rsType/bus/to/73.447734,61.256534╎Evolution Fit?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти
            проезд до Evolution Fit</a></div>
    <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
    <script charset="utf-8">
        new DGWidgetLoader({
            "pos": {"lat": 61.256534, "lon": 73.447734, "zoom": 16},
            "opt": {"city": "surgut"},
            "org": [{"id": "70000001034922542"}]
        });</script>
    <noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите
        его в
        настройках вашего браузера.
    </noscript>
</div>