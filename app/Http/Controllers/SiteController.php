<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers;


use App\Models\GalleryItem;
use App\Models\Promo;
use App\Models\Schedule;
use App\Models\Trainer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class SiteController extends Controller
{

    public function index()
    {
        $today = Carbon::today('Asia/Yekaterinburg');
        $promos = Promo::with('image')->active()->get();
        $galleryItems = GalleryItem::all();
        return view('index', [
            'promos' => $promos,
            'galleryItems' => $galleryItems,
            'schedules' => Schedule::schedulesForDate($today),
        ]);
    }

    public function schedule(Request $r)
    {
        $today = Carbon::today('Asia/Yekaterinburg');

        $date = $r->date ? Carbon::parse($r->date, 'Asia/Yekaterinburg') : $today;

        $carbonPeriod = $today->daysUntil(Carbon::today('Asia/Yekaterinburg')->endOfMonth());
        $sliderDates = [];
        foreach ($carbonPeriod as $d) {
            array_push($sliderDates, [
                'iso' => $d->format('d-m-Y'),
                'dayOfWeek' => $d->formatLocalized('%A'),
                'date' => $d->formatLocalized('%d %B'),
                'active' => $d->diffInDays($date) ? false : true
            ]);
        }

        return view('schedule', [
            'schedules' => Schedule::schedulesForDate($date, $r->group),
            'sliderDates' => $sliderDates
        ]);
    }

    public function promos()
    {
        $promos = Promo::all();
        return view('promos', [
            'promos' => $promos,
        ]);
    }

    public function cards()
    {
        return view('cards');
    }

    public function trainers()
    {
        $trainers = Trainer::with(['image'])->get();
        return view('trainers', ['trainers' => $trainers]);
    }

    public function contacts()
    {
        return view('contacts');
    }
}
