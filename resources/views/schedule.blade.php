@extends('layouts.main', ['bodyClass'=>'p-index'])

@push('head')
    <title>Расписание занятий</title>
@endpush

@section('content')
    <div class="container mb-4">
        @include('components.breadcrumbs',['items'=> [
        ['name'=>'Главная','url'=>'/'],
        ['name'=>'Расписание','url'=>route('schedule')],
        ]])
    </div>
    @include('components.schedule', ['class'=>'', 'header'=>'Расписание'])
@endsection