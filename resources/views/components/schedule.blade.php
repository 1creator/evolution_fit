<div class="{{$class??'pt-9'}}">
    <div class="container">
        <h2>{{$header??'Расписание на сегодня'}}</h2>
        <div class="tabs">
            <a class="tabs__item {{!request()->has('group')?'active':''}}"
               href="{{route('schedule',['date'=>request('date')])}}">Все страницы</a>
            @foreach(\App\Models\Group::all() as $group)
                <a class="tabs__item {{(request()->group==$group->id)?'active':''}}"
                   href="{{route('schedule', ['group'=>$group->id, 'date'=>request('date')])}}">{{$group->name}}</a>
            @endforeach
        </div>
    </div>
    <div class="schedule">
        <div class="container pb-9">
            @if(isset($sliderDates))
                <div class="date-slider-wrap">
                    <div class="swiper-container date-slider mb-4" data-preset="1">
                        <div class="swiper-wrapper">
                            @foreach($sliderDates as $date)
                                <div class="swiper-slide">
                                    <a href="{{route('schedule',[
                                            'group'=>request('group'),
                                            'date'=>$date['iso']
                                            ])}}"
                                       class="date-slider-item {{$date['active']?'active':''}}">
                                        <div class="date-slider-item__week-day">
                                            {{$date['dayOfWeek']}}</div>
                                        <div>{{$date['date']}}</div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <div class="mb-7">
                <div class="h3 mb-5"><span class="border_bottom mt-5">Утро</span></div>
                <div class="row flex-wrap">
                    @foreach($schedules['morning'] as $schedule)
                        <div class="col-12 col-md-4 col-lg-3 mb-4">
                            @include('components.schedule-card', ['schedule'=>$schedule])
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="mb-7">
                <div class="h3 mb-5"><span class="border_bottom">День</span></div>
                <div class="row flex-wrap">
                    @foreach($schedules['day'] as $schedule)
                        <div class="col-12 col-md-4 col-lg-3 mb-4">
                            @include('components.schedule-card', ['schedule'=>$schedule])
                        </div>
                    @endforeach
                </div>
            </div>
            <div>
                <div class="h3 mb-5"><span class="border_bottom">Вечер</span></div>
                <div class="row flex-wrap">
                    @foreach($schedules['evening'] as $schedule)
                        <div class="col-12 col-md-4 col-lg-3 mb-4">
                            @include('components.schedule-card', ['schedule'=>$schedule])
                        </div>
                    @endforeach
                </div>
            </div>

            @if($showRouteButton??false)
                <div class="text-center">
                    <a class="btn btn_dark" href="{{route('schedule')}}">Смотреть другие дни</a>
                </div>
            @endif
        </div>
    </div>
</div>
