<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAdminAccess;
use App\Models\Trainer;
use Illuminate\Http\Request;

class TrainerController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', CheckAdminAccess::class]);
    }

    public function index(Request $r)
    {
        $result = Trainer::requested($r)->get();
        return ['response' => $result];
    }

    public function store(Request $r)
    {
        $this->validate($r, [
            'first_name' => 'string|required|max:255',
            'last_name' => 'sometimes|string|required|max:255',
            'description' => 'required',
            'image_id' => 'nullable|exists:attachments,id',
        ]);

        $result = Trainer::create($r->all());
        return ['response' => $result];
    }

    public function show(Request $r, Trainer $trainer)
    {
        $trainer->loadRequested($r);
        return ['response' => $trainer];
    }

    public function destroy(Request $r, Trainer $trainer)
    {
        $trainer->delete();
        return ['response' => 1];
    }

    public function update(Request $r, Trainer $trainer)
    {
        $trainer->update($r->only($trainer->getFillable()));
        return ['response' => $trainer];
    }
}
