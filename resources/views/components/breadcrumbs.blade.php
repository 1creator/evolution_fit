<ol class="mb-5 breadcrumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
    @foreach($items as $index=>$item)
        @if($index+1 != count($items))
            <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                <a itemprop="item" title="{{$item['name']}}" href="{{$item['url']}}">
                    <span itemprop="name">{{$item['name']}}</span>
                    <meta itemprop="position" content="{{$index+1}}">
                </a>
            </li>
        @endif
    @endforeach
    <li title="{{$items[count($items)-1]['name']}}">
        <span>{{$items[count($items)-1]['name']}}</span>
    </li>
</ol>