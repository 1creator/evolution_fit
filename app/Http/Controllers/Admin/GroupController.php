<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAdminAccess;
use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', CheckAdminAccess::class]);
    }

    public function index(Request $r)
    {
        $result = Group::requested($r)->get();
        return ['response' => $result];
    }

    public function store(Request $r)
    {
        $this->validate($r, [
            'name' => 'string|required|max:255',
        ]);

        $result = Group::create($r->all());

        $result->trainings()->sync($r->training_ids);

        return ['response' => $result];
    }

    public function show(Request $r, Group $group)
    {
        $group->loadRequested($r);
        return ['response' => $group];
    }

    public function destroy(Request $r, Group $group)
    {
        $group->delete();
        return ['response' => 1];
    }

    public function update(Request $r, Group $group)
    {
        $group->update($r->only($group->getFillable()));
        $group->trainings()->sync($r->training_ids);
        return ['response' => $group];
    }
}