<div class="promos {{$class??'py-5'}}">
    <div class="container">
        <div class="swiper-container">
            <div class="swiper-wrapper align-items-center">
                @foreach($promos as $promo)
                    <div class="swiper-slide">
                        <div class="promo">
                            <div class="row">
                                <div class="col-12 col-md-5 d-flex align-items-center">
                                    <img class="promo__image"
                                         src="{{asset($promo->image->url)}}">
                                </div>
                                <div class="col-12 col-md-7 d-flex flex-column align-items-start justify-content-center">
                                    <div class="bg-brush">
                                        <div class="h3">{{$promo->name}}</div>
                                    </div>
                                    <div class="mb-4">
                                        {!! $promo->description !!}
                                    </div>
                                    {{--<button class="btn btn_dark">Подать заявку</button>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>
