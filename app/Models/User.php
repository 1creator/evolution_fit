<?php

namespace App\Models;

use App\Utils\RequestedScope;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends RequestedScope implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail;
    use Notifiable;

    protected $available_with = ['staff', 'staff.permissions'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected
        $fillable = [
        'first_name', 'last_name', 'middle_name', 'address',
        'email', 'password', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected
        $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected
        $casts = [
        'email_verified_at' => 'datetime',
    ];

    public
    function staff()
    {
        return $this->hasOne(Staff::class);
    }

    public
    function isStaff()
    {
        return $this->staff()->exists();
    }

    public
    function getFullNameAttribute()
    {
        return $this->second_name . ' ' . $this->first_name . ' ' . $this->middle_name;
    }

    public
    function permissions()
    {
        if ($this->isStaff()) {
            if ($this->email == 'admin@ra-bukva.ru') {
                return Permission::all();
            } else {
                return $this->staff->permissions;
            }
        } else {
            return null;
        }
    }
}
