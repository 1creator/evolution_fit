import VueRouter from "vue-router";
import Vue from "vue";

import ThePromos from "../components/admin/ThePromos";
import ThePromoEditor from "../components/admin/ThePromoEditor";

import TheGroups from "../components/admin/TheGroups";
import TheGroupEditor from "../components/admin/TheGroupEditor";

import TheProducts from "../components/admin/TheProducts";
import TheProductEditor from "../components/admin/TheProductEditor";

import TheTrainers from "../components/admin/TheTrainers";
import TheTrainerEditor from "../components/admin/TheTrainerEditor";

import TheTrainings from "../components/admin/TheTrainings";
import TheTrainingEditor from "../components/admin/TheTrainingEditor";
import TheGallery from "../components/admin/TheGallery";

Vue.use(VueRouter);

let routes = [
    {
        component: ThePromos,
        path: '/admin/promos',
        name: 'promos',
        props: true,
        meta: {title: 'Акции | Панель управления'}
    },
    {
        component: ThePromoEditor,
        path: '/admin/promos/create',
        name: 'promos.create',
        props: true,
        meta: {title: 'Создание акции | Панель управления'}
    },
    {
        component: ThePromoEditor,
        path: '/admin/promos/:promoId/edit',
        name: 'promos.edit',
        props: true,
        meta: {title: 'Редакторование акции | Панель управления'}
    },

    {
        component: TheGroups,
        path: '/admin/groups',
        name: 'groups',
        props: true,
        meta: {title: 'Группы | Панель управления'}
    },
    {
        component: TheGroupEditor,
        path: '/admin/groups/create',
        name: 'groups.create',
        props: true,
        meta: {title: 'Создание группы | Панель управления'}
    },
    {
        component: TheGroupEditor,
        path: '/admin/groups/:groupId/edit',
        name: 'groups.edit',
        props: true,
        meta: {title: 'Редактирование группы | Панель управления'}
    },

    {
        component: TheProducts,
        path: '/admin/products',
        name: 'products',
        props: true,
        meta: {title: 'Товары | Панель управления'}
    }, {
        component: TheProductEditor,
        path: '/admin/products/create',
        name: 'products.create',
        props: true,
        meta: {title: 'Создание товара | Панель управления'}
    },
    {
        component: TheProductEditor,
        path: '/admin/products/:productId/edit',
        name: 'products.edit',
        props: true,
        meta: {title: 'Редакторование товара | Панель управления'}
    },

    {
        component: TheTrainers,
        path: '/admin/trainers',
        name: 'trainers',
        props: true,
        meta: {title: 'Тренеры | Панель управления'}
    },
    {
        component: TheTrainerEditor,
        path: '/admin/trainers/create',
        name: 'trainers.create',
        props: true,
        meta: {title: 'Добавление тренера | Панель управления'}
    },
    {
        component: TheTrainerEditor,
        path: '/admin/trainers/:trainerId/edit',
        name: 'trainers.edit',
        props: true,
        meta: {title: 'Редактирование тренера | Панель управления'}
    },

    {
        component: TheTrainings,
        path: '/admin/trainings',
        name: 'trainings',
        props: true,
        meta: {title: 'Занятия | Панель управления'}
    },
    {
        component: TheTrainingEditor,
        path: '/admin/trainings/create',
        name: 'trainings.create',
        props: true,
        meta: {title: 'Создание занятия | Панель управления'}
    },
    {
        component: TheTrainingEditor,
        path: '/admin/trainings/:trainingId/edit',
        name: 'trainings.edit',
        props: true,
        meta: {title: 'Редакторование занятия | Панель управления'}
    },

    {
        component: TheGallery,
        path: '/admin/gallery',
        name: 'gallery',
        props: true,
        meta: {title: 'Галерея | Панель управления'}
    },
];

export const router = new VueRouter({
    mode: 'history',
    routes: routes
});

router.beforeEach((to, from, next) => {
    document.title = to.meta.title || document.title;
    next();
});
