import {initSwipers} from "./utils/initSwipers";

require('bootstrap/js/dist/modal');

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./axios');
import axios from 'axios';

document.addEventListener("DOMContentLoaded", initSwipers);

let axiosForms = document.getElementsByClassName('ajax-form');
for (let form of axiosForms) {
    form.addEventListener('submit', function (event) {
        event.preventDefault();

        this.classList.toggle('ajax-form_loading', true);

        let url = this.getAttribute('action');
        let data = new FormData(this);
        axios.post(url, data).then(res => {
            this.classList.toggle('ajax-form_loading', false);
            this.classList.toggle('ajax-form_submitted', true);
        });
    });
}
