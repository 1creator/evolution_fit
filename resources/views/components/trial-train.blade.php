<div class="container">
    <h3>Записаться на <span class="text-primary">пробное занятие</span></h3>
    <form class="mb-5 ajax-form"
          action="{{route('cards.trial')}}" method="post">
        <div class="ajax-form__form">
            <div class="row">
                <div class="col-12 col-md">
                    <div class="input">
                        <label for="last-name">Фамилия</label>
                        <input type="text" name="last_name" id="last-name" required>
                    </div>
                </div>
                <div class="col-12 col-md">
                    <div class="input">
                        <label for="first-name">Имя</label>
                        <input type="text" name="first_name" id="first-name" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="input">
                        <label for="birthday">Дата рождения</label>
                        <input type="date" name="birthday" id="birthday" required>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input">
                        <label for="phone">Телефон</label>
                        <input type="tel" name="phone" id="phone" required>
                    </div>
                </div>
            </div>
            <div class="input-cb">
                <input required type="checkbox" name="terms" id="terms">
                <label for="terms">Я даю согласие на обработку моих персональных данных</label>
            </div>
            <button class="btn btn_primary" type="submit">Отправить заявку</button>
        </div>
        <div class="ajax-form__conclusion">
            <div class="h3">Спасибо!</div>
            <div>Ваша заявка на пробное занятие оформлена. Мы скоро вам перезвоним.</div>
        </div>
    </form>
</div>