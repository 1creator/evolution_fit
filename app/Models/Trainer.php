<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Services\Attachment\Models\Attachment;
use App\Utils\RequestedScope;

class Trainer extends RequestedScope
{
    protected $fillable = ['first_name', 'last_name', 'middle_name', 'description', 'image_id', 'position'];
    protected $able_with = ['image'];

    public function image()
    {
        return $this->belongsTo(Attachment::class, 'image_id');
    }
}
