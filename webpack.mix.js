const webpack = require('webpack');
const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix
    .webpackConfig({
        plugins: [
            new webpack.ContextReplacementPlugin(
                /moment[\/\\]locale/,
                // A regular expression matching files that should be included
                /(ru)\.js/
            )
        ]
    })
    .setPublicPath('public_html')
    .options({processCssUrls: false})
    .version()
    .js('resources/js/app.js', 'public_html/js')
    .js('resources/js/admin.js', 'public_html/js')
    .sass('resources/sass/vendor.scss', 'public_html/css')
    .sass('resources/sass/admin.scss', 'public_html/css')
    .sass('resources/sass/app.scss', 'public_html/css');