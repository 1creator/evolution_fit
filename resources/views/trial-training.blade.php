@extends('layouts.main', ['bodyClass'=>'p-index'])

@push('head')
    <title>Клубные карты</title>
@endpush

@section('content')
    <div class="container mb-4">
        @include('components.breadcrumbs',['items'=> [
        ['name'=>'Главная','url'=>'/'],
        ['name'=>'Клубные карты','url'=>route('cards')],
        ['name'=>'Пробное занятие','url'=>route('cards.trial')],
        ]])
    </div>
    <div class="container">
        <div class="h2 mb-5">Виды клубных карт</div>
        <div class="tabs mb-5">
            <a href="{{route('cards')}}" class="tabs__item">Виды клубных карт</a>
{{--            <a href="{{route('cards.freezing')}}" class="tabs__item">Заморозка карты</a>--}}
            <a href="{{route('cards.trial')}}" class="tabs__item active">Пробное занятие</a>
        </div>
    </div>
    @include('components.trial-train')
@endsection
