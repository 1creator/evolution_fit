import axios from "axios";
import moment from "moment";
import {toCamelCaseKeys} from "../utils/toCamelCase";

axios.defaults.transformResponse = [(data) => {
    data = JSON.parse(data);
    return toCamelCaseKeys(data, {deep: true});
}];

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (401 === error.response.status) {
        window.location = "/login";
    } else {
        return Promise.reject(error);
    }
});

export let promos = {
    fetch: function (params) {

        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/promos`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    get: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/promos/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            axios
                .post(`/ajax/admin/promos`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .put(`/ajax/admin/promos/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/ajax/admin/promos/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
};

export let trainers = {
    fetch: function (params) {
        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/trainers`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    get: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/trainers/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            axios
                .post(`/ajax/admin/trainers`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .put(`/ajax/admin/trainers/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/ajax/admin/trainers/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
};

export let galleryItems = {
    fetch: function (params) {
        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/gallery-items`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    get: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/gallery-items/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            axios
                .post(`/ajax/admin/gallery-items`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .put(`/ajax/admin/gallery-items/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    sync: function (params) {
        return new Promise((resolve, reject) => {
            axios
                .put(`/ajax/admin/gallery-items/sync`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/ajax/admin/gallery-items/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
};

export let products = {
    fetch: function (params) {

        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/products`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    get: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/products/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            axios
                .post(`/ajax/admin/products`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .put(`/ajax/admin/products/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/ajax/admin/products/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
};

export let trainings = {
    fetch: function (params) {

        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/trainings`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    get: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/trainings/${id}`, {params: params})
                .then(response => {
                    let training = response.data.response;
                    if (training.schedules) {
                        training.schedules.forEach((item) => {
                            item.openTime = moment.utc(item.openTime, "HH:mm").local().format("HH:mm:ss");
                            item.closeTime = moment.utc(item.closeTime, "HH:mm").local().format("HH:mm:ss");
                        });
                    }

                    resolve(training);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        params.schedules.forEach((item) => {
            item.open_time = moment(item.open_time, "HH:mm").utc().format("HH:mm:ss");
            item.close_time = moment(item.close_time, "HH:mm").utc().format("HH:mm:ss");
        });

        return new Promise((resolve, reject) => {
            axios
                .post(`/ajax/admin/trainings`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        params.schedules.forEach((item) => {
            item.open_time = moment(item.open_time, "HH:mm").utc().format("HH:mm:ss");
            item.close_time = moment(item.close_time, "HH:mm").utc().format("HH:mm:ss");
        });

        return new Promise((resolve, reject) => {
            axios
                .put(`/ajax/admin/trainings/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/ajax/admin/trainings/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
};

export let groups = {
    fetch: function (params) {

        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/groups`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    get: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .get(`/ajax/admin/groups/${id}`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        return new Promise((resolve, reject) => {
            axios
                .post(`/ajax/admin/groups`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .put(`/ajax/admin/groups/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/ajax/admin/groups/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
};
