@extends('layouts.main', ['bodyClass'=>'p-index'])

@push('head')
    <title>Клубные карты</title>
@endpush

@section('content')
    <div class="container mb-4">
        @include('components.breadcrumbs',['items'=> [
        ['name'=>'Главная','url'=>'/'],
        ['name'=>'Клубные карты','url'=>route('cards')],
        ['name'=>'Заморозка карты','url'=>route('cards.freezing')],
        ]])
    </div>
    <div class="container">
        <div class="h2 mb-5">Виды клубных карт</div>
        <div class="tabs mb-5">
            <a href="{{route('cards')}}" class="tabs__item">Виды клубных карт</a>
            <a href="{{route('cards.freezing')}}" class="tabs__item active">Заморозка карты</a>
            <a href="{{route('cards.trial')}}" class="tabs__item">Пробное занятие</a>
        </div>
        <form class="mb-5 ajax-form" action="{{route('cards.freezing')}}" method="post">
            <div class="ajax-form__form">
                {{csrf_field()}}
                <div class="h5">Информация о владельце</div>
                <div class="row">
                    <div class="col-12 col-md">
                        <div class="input">
                            <label for="last-name">Фамилия</label>
                            <input type="text" name="last_name" id="last-name" required>
                        </div>
                    </div>
                    <div class="col-12 col-md">
                        <div class="input">
                            <label for="first-name">Имя</label>
                            <input type="text" name="first_name" id="first-name" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="input">
                            <label for="birthday">Дата рождения</label>
                            <input type="date" name="birthday" id="birthday" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="input">
                            <label for="phone">Телефон</label>
                            <input type="tel" name="phone" id="phone" required>
                        </div>
                    </div>
                </div>
                <div class="h5">Период заморозки</div>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="input">
                            <label for="from-date">С</label>
                            <input type="date" name="from_date" id="from-date" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="input">
                            <label for="to-date">По</label>
                            <input type="date" name="to_date" id="to-date" required>
                        </div>
                    </div>
                </div>
                <div class="input-cb">
                    <input type="checkbox" name="terms" id="terms" required>
                    <label for="terms">Я даю согласие на обработку моих персональных данных</label>
                </div>
                <button class="btn btn_primary" type="submit">Отправить</button>
            </div>
            <div class="ajax-form__conclusion">
                <div class="h3">Спасибо!</div>
                <div>Ваша заявка на заморозку клубной карты оформлена. Мы скоро вам перезвоним.</div>
            </div>
        </form>
    </div>
@endsection