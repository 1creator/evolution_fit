import {Mark} from 'tiptap'
import {toggleMark} from 'tiptap-commands'

export default class TipTapButton extends Mark {

    // choose a unique name
    get name() {
        return 'button'
    }

    // the prosemirror schema object
    // take a look at https://prosemirror.net/docs/guide/#schema for a detailed explanation
    get schema() {
        return {
            // content: 'text*',
            // inline: true,
            // group: 'inline',
            // group: 'block',
            // defining: true,
            // draggable: false,
            // selectable: true,
            // define how the editor will detect your node from pasted HTML
            // every blockquote tag will be converted to this blockquote node
            inclusive: false,
            parseDOM: [
                {tag: 'button'},
            ],
            // this is how this node will be rendered
            // in this case a blockquote tag with a class called `awesome-blockquote` will be rendered
            // the '0' stands for its text content inside
            toDOM: () => ['button', {class: 'btn btn_primary', type: 'button'}, 0],
        }
    }

    // this command will be called from menus to add a blockquote
    // `type` is the prosemirror schema object for this blockquote
    // `schema` is a collection of all registered nodes and marks
    commands({type, schema}) {
        return () => toggleMark(type)
    }
}
