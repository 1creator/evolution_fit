<?php

use App\Models\Permission;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InsertAdminModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create([
            'id' => 'admin.access',
            'name' => 'Админ панель.Доступ',
        ]);

        $user = User::create([
            'email' => 'superadmin@1creator.ru',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'
        ]);
        $staff = $user->staff()->create(['position' => 'Суперадмин']);
        $staff->permissions()->attach('admin.access');


        $user = User::create([
            'email' => 'evolution.fit.18@inbox.ru',
            'password' => \Illuminate\Support\Facades\Hash::make('5A#aCX')
        ]);
        $staff = $user->staff()->create(['position' => 'Администратор']);
        $staff->permissions()->attach('admin.access');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::where('email', 'superadmin@evo-fit.club')->delete();
        \Illuminate\Support\Facades\DB::table('staff_permission')->truncate();
        \Illuminate\Support\Facades\DB::table('permissions')->truncate();
    }
}
