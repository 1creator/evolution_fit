@extends('layouts.main', ['bodyClass'=>'p-index'])

@push('head')
    <title>Клубные карты</title>
@endpush

@section('content')
    <div class="container mb-4">
        @include('components.breadcrumbs',['items'=> [
        ['name'=>'Главная','url'=>'/'],
        ['name'=>'Клубные карты','url'=>route('cards')],
        ]])
    </div>
    <div class="container">
        <div class="h2 mb-5">Виды клубных карт</div>
        <div class="tabs mb-5">
            <a href="{{route('cards')}}" class="tabs__item active">Виды клубных карт</a>
{{--            <a href="{{route('cards.freezing')}}" class="tabs__item">Заморозка карты</a>--}}
            <a href="{{route('cards.trial')}}" class="tabs__item">Пробное занятие</a>
        </div>
        <div class="row">
            @foreach($products as $product)
                <div class="col-12 col-md-4 mb-4">
                    <div class="card-product">
                        <div class="card-product__img">
                            <img src="{{asset($product->image->url ?? 'images/logo-small.svg')}}">
                        </div>
                        <div class="card-product__text">
                            <div class="card-product__name">{{$product->name}}</div>
                            <div class="card-product__price">Цена: <span class="text-primary">{{$product->price}}
                                    ₽</span></div>
                            <div class="card-product__description">
                                {!! $product->description !!}
                                <button type="button" class="btn btn_dark"
                                        data-toggle="modal" data-target="#callback-modal">Хочу!</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
