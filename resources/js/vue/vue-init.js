import Vue from "vue";
import {router} from "./routing/router";
import TheApp from "./components/admin/TheApp";

window["VueApp"] = new Vue({
    router: router,
    el: "#app",
    components: {},
    render: h => h(TheApp),
});
