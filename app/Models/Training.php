<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Services\Attachment\Models\Attachment;
use App\Utils\RequestedScope;

class Training extends RequestedScope
{
    protected $fillable = ['name', 'description', 'image_id'];
    protected $able_with = ['image', 'schedules'];

    public function image()
    {
        return $this->belongsTo(Attachment::class, 'image_id');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class)->orderBy('day')->orderBy('open_time');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_training');
    }
}
