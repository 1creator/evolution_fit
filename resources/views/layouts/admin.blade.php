@extends('layouts.main',['bodyClass'=>'d-flex flex-column'])

@push('head')
    <link href="{{mix('css/admin.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div id="app"></div>
@endsection

@section('footer')
@stop

@push('scripts')
    <script src="{{mix('js/admin.js')}}"></script>
@endpush
