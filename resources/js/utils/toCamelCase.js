const toCamel = (s) => {
    return s.replace(/([-_][a-z])/ig, ($1) => {
        return $1.toUpperCase()
            .replace('-', '')
            .replace('_', '');
    });
};

const isObject = function (o) {
    return o === Object(o) && !isArray(o) && typeof o !== 'function';
};

const isArray = function (o) {
    return Array.isArray(o);
};

export function toCamelCaseKeys(o, options) {
    if (isObject(o)) {
        const n = {};

        Object.keys(o)
            .forEach((k) => {
                n[toCamel(k)] = toCamelCaseKeys(o[k]);
            });

        return n;
    } else if (isArray(o)) {
        return o.map((i) => {
            return toCamelCaseKeys(i);
        });
    }

    return o;
}