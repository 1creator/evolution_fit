<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAdminAccess;

class ScheduleController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', CheckAdminAccess::class]);
    }

    public function index()
    {
        return view('admin.schedule');
    }
}