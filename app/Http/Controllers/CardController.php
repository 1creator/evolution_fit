<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers;


use App\Models\Product;

class CardController extends Controller
{

    public function index()
    {
        $products = Product::with(['image'])->get();
        return view('cards', ['products' => $products]);
    }

    public function freezing()
    {
        return view('card-freezing');
    }

    public function trial()
    {
        return view('trial-training');
    }
}