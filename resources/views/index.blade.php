@extends('layouts.main', ['bodyClass'=>'p-index'])

@push('head')
    <title>Evolution Fit - фитнес клуб в Сургуте</title>
@endpush

@section('content')
    <div class="p-index__entry text-white py-9">
        <div class="container">
            <h1>
                <span class="text-primary">Evolution Fit</span>
                <span class="d-block" style="font-size: 36px;line-height: 1">Фитнес клуб в Сургуте</span>
            </h1>
            <h2>
                Доступный спорт<br/>в комфортном зале
            </h2>
            <ul class="list-unstyled contacts-group">
                <li>
                    <i class="material-icons">
                        location_on
                    </i>
                    г. Сургут, ул. 30 лет Победы, д. 66
                </li>
                <li>
                    <i class="material-icons">
                        phone
                    </i>
                    <a class="pr-0"
                       href="tel:93-55-50">93-55-50</a> / <a class="pl-0"
                                                             href="tel:93-55-59">93-55-59</a>
                </li>
                <li>
                    <i class="material-icons">
                        alternate_email
                    </i>
                    <a href="mailto:evolution.fit.18@inbox.ru">evolution.fit.18@inbox.ru</a>
                </li>
            </ul>
        </div>
    </div>

    @if(count($promos))
        @include('components.promos')
        <div class="mb-n10"></div>
    @endif

    @include('components.schedule', ['showRouteButton'=>true])
    <div class="p-index__gym">
        <div class="container py-9">
            <h2>Что такое<br/>
                <span class="text-primary">Evolution fit?</span>
            </h2>
            <div class="text-right">
                <h3 class="h2 lined-header lined-header__right text-primary">
                    Комфортный зал
                    <span class="lined-header__number">1</span>
                </h3>
                <div class="row">
                    <div class="col-12 col-md-8 offset-md-4">
                        <p>
                            Клуб, <strong>общей площадью более 3000&nbsp;м<sup>2</sup></strong> позволил собрать все
                            основные направления занятий спортом и разместить достаточное количество
                            оборудования для избежания очередей.
                        </p>
                        <ul>
                            <li>Большая кардио-зона с электрическими беговыми дорожками и дорожками с магнитным беговым
                                полотном, эллипсоиды, велотренажеры, гребля, степперы;
                            </li>
                            <li>Зал силовых тренировок с высококачественным спортивным оборудованием;
                            </li>
                            <li>Зона единоборств площадью 480 м<sup>2</sup>;</li>
                            <li>3 студии групповых тренировок, укомплектованых новым оборудованием, свободными весами,
                                Step-платформами и ковриками.
                            </li>
                            <li>
                                Зона смешанных боевых искусств со всеми условиями для тренировок и спаррингов.
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-index__flexible-prices">
        <div class="container py-9">
            <div>
                <h3 class="h2 lined-header">
                    <span class="lined-header__number">2</span>
                    Гибкие цены
                </h3>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <p>
                            Наш фитнес клуб отличается не только достойным уровнем сервиса, но и демократичными ценами.
                        </p>
                        <p>Мы
                           убеждены в том, что <strong>качественный фитнес должен быть доступен каждому
                                                       желающему</strong>.
                           Именно поэтому, наши клубные карты являются выгодным предложением
                           для совершенствования тела и укрепления здоровья. </p>
                        <p class="mb-3">
                            Для того, чтобы удовлетворить потребности всех наших клиентов,
                            мы не только создали разнообразные варианты клубных карт,
                            но и ввели дополнительную систему лояльности - вы можете получить
                            приятную скидку уже при первой покупке абонемента.
                        </p>
                        <p>
                            При всём этом, качество наших тренировок гарантированно остается на самом высшем уровне.
                        </p>
                        <a href="{{route('cards')}}" class="btn btn_primary">Посмотреть клубные карты</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-index__trainers">
        <div class="container py-9">
            <div class="text-right">
                <h3 class="h2 lined-header  lined-header__right text-primary">
                    Опытные тренеры
                    <span class="lined-header__number">3</span>
                </h3>
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-6">
                        <p>
                            Правильно составленная программа тренировок и корректность её выполнения – залог успешного
                            достижения наилучших результатов.
                            <a class="text-primary" href="{{route('trainers')}}">Более 20 опытных тренеров</a> помогут
                            Вам составить
                            индивидуальный
                            комплекс тренировок, с учётом ваших характеристик и желаемых результатов.
                        </p>
                        <p>Наши тренеры и инструкторы будут для Вас не только профессиональными наставниками, но
                           и станут друзьями и товарищами, которые помогут Вам добиться желаемых результатов в
                           обозначенные
                           сроки.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-index__service">
        <div class="container py-9">
            <div>
                <h3 class="h2 lined-header">
                    <span class="lined-header__number">4</span>
                    Качественный сервис
                </h3>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <p>
                            На территории нашего клуба можно воспользоваться следующими услугами:
                        </p>
                        <ul>
                            <li>
                                <h5>Фитнес-бар</h5>
                                <div>
                                    зона релаксации, где вы можете набраться энергии и восстановить потерянные витамины
                                    и минералы.
                                </div>
                            </li>
                            <li>
                                <h5>Спортивный магазин</h5>
                                <div>
                                    консультанты магазина всегда готовы помочь в выборе спортивного питания или
                                    подходящего
                                    домашнего инвентаря.
                                </div>
                            </li>
                            <li>
                                <h5>Детская комната</h5>
                                <div>
                                    Увлекательное занятие для ребёнка во время Вашей тренировки.
                                </div>
                            </li>
                            <li>
                                <h5>SPA-зона</h5>
                                <div>
                                    солярий, услуги маникюра, депиляции, все виды массажа и многое другое станут
                                    приятным
                                    дополнением к посещению фитнес клуба.
                                </div>
                            </li>
                            <li>
                                <h5>Турецкий хамам (мужской и женский)</h5>
                                <div>
                                    Турецкая баня поможет вам расслабиться после интенсивной тренировки, улучшит
                                    кровообращение и
                                    укрепит организм в целом.
                                </div>
                            </li>
                            <li>
                                <h5>Мощный солярий с новыми лампами</h5>
                                <div>
                                    Подчеркнуть результаты тренировок можно с помощью ровного загара, полученного в
                                    нашем солярии.
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id="les-milss"></a>
    <div class="p-index__les-mills">
        <div class="container mt-9 d-flex flex-column">
            <div class="row no-gutters">
                <div class="block-header block-header_right col-lg-7 offset-lg-5">
                    <span class="h2 mt-0 block-header__num">5</span>
                    <div class="d-flex flex-column">
                        <h3 class="h2">LES MILLS</h3>
                        <span class="block-header__sub">официальные  представители направления</span>
                    </div>
                </div>
            </div>

            <div class="row no-gutters">
                <div class="col-12 col-lg-7 offset-lg-5">
                    <p>Самый популярный вид фитнеса по версии книги Гинесса. <br>
                        <b>6 программ тренировок:</b></p>

                    <div class="d-flex flex-wrap">
                        <div class="les-mills__activity">
                            <span class="activity__label">BODYBALANCE</span>
                            <p>Тренировка, основанная на упражнениях йоги и направленная на развитие гибкости и
                               укрепление мышечного тонуса</p>
                        </div>
                        <div class="les-mills__activity">
                            <span class="activity__label">THE TRIP</span>
                            <p>40 минутная сайкл-тренировка с погружением в цифровой мир с помощью большого экрана и
                               мощной акустической системы </p>
                        </div>
                        <div class="les-mills__activity">
                            <span class="activity__label">SPRINT</span>
                            <p>Высокоинтенсивная интервальная тренировка на велотренажере</p>
                        </div>
                        <div class="les-mills__activity">
                            <span class="activity__label">CXWORX</span>
                            <p>30 минутная тренировка основных групп мышц</p>
                        </div>
                        <div class="les-mills__activity">
                            <span class="activity__label">BODYPUMP</span>
                            <p>Силовая тренировка со штангой</p>
                        </div>
                        <div class="les-mills__activity">
                            <span class="activity__label">GRIT</span>
                            <p>Серия нескольких 30 минутных высокоинтенсивных тренировок</p>
                        </div>
                        <button class="btn btn_primary les-mills__button"
                                data-toggle="modal" data-target="#callback-modal"
                        >Подать заявку
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-index__children-activities mt-n1">
        <div class="container d-flex flex-column">
            <div class="row no-gutters">
                <div class="block-header">
                    <span class="h2 mt-0 block-header__num">6</span>
                    <div class="d-flex flex-column">
                        <h3 class="h2">Занятия для детей</h3>
                        <span class="block-header__sub">Evolution Fit не только для взрослых!</span>
                    </div>
                </div>
            </div>

            <div class="row no-gutters">
                <span class="h3 text-left w-100">Единоборства</span>
                <div class="col-12 col-lg-8 offset-lg-4 children-activities__fighters">
                    <div class="children-activities__card">
                        <div class="card__label">Тайский бокс</div>
                        Единоборство, включающее в себя ударную технику ногами и руками
                        <div class="children-activities__card-age">С 7 лет</div>
                    </div>
                    <div class="children-activities__card">
                        <div class="card__label">Кикбоксинг</div>
                        Единоборство, включающее в себя ударную технику
                        ногами и руками
                        <div class="children-activities__card-age">С 6 лет</div>
                    </div>
                    <div class="children-activities__card">
                        <div class="card__label">MMA kids</div>
                        Детские занятия по смешанным единоборствам под наблюдением профессиональных тренеров
                        <div class="children-activities__card-age">С 3 лет</div>
                    </div>
                    <div class="children-activities__card">
                        <div class="card__label">Бокс</div>
                        Постановка техники удара руками, защиты корпуса под руководством профессионального тренера
                        <div class="children-activities__card-age">С 7 лет</div>
                    </div>
                </div>
                <span class="h3 text-right w-100 mt-7">Авторские курсы</span>
                <div class="col-12 col-lg-8 children-activities__programs ">
                    <div class="children-activities__card">
                        <div class="card__label">Crossfit kids</div>
                        Курс, включающий в себя самые легкие базовые упражнения кроссфита.
                        <div class="children-activities__card-age">С 8 лет</div>
                    </div>
                    <div class="children-activities__card">
                        <div class="card__label">Fitness kids</div>
                        Курс основывается на различных комплексах тренировок с привлечением тренеров по растяжке и
                        единоборствам.
                        <div class="children-activities__card-age">С 10 лет</div>
                    </div>
                    <!-- <div class="children-activities__card">
                        <div class="card__label">Streching kids</div>
                        Растяжка для детей с пользой для здоровья,направленная на улучшение гибкости всего тела
                        <div class="children-activities__card-age">С 6 лет</div>
                    </div> -->
                    <div class="children-activities__card">
                        <div class="card__label">Юный богатырь</div>
                        Авторский детский и подростковый курс,основанный на занятиях в фитнес-зале.
                        <div class="children-activities__card-age">С 8 лет</div>
                    </div>
                    <div class="children-activities__card">
                        <div class="card__label">Дошколята</div>
                        Развитие и укрепление опорно-двигательной системы, координации, логического мышления и социальных навыков.
                        <div class="children-activities__card-age">С 4 до 8 лет</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-index__gallery pb-5">
        <div class="container">
            <h3><span class="text-primary">Evolution Fit</span> в картинках</h3>
            @include('components.progress-gallery')
        </div>
    </div>
    <div class="py-7">
        @include('components.trial-train')
    </div>
@endsection
