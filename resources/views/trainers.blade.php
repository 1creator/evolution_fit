@extends('layouts.main', ['bodyClass'=>'p-index'])

@push('head')
    <title>Тренеры и инструкторы</title>
@endpush

@section('content')
    <div class="container mb-4">
        @include('components.breadcrumbs',['items'=> [
        ['name'=>'Главная','url'=>'/'],
        ['name'=>'Тренеры','url'=>route('trainers')],
        ]])
        <h1 class="h2 mb-5">Тренеры</h1>
        <div class="row">
            @foreach($trainers as $trainer)
                <div class="col-12 col-md-4 mb-4">
                    <div class="card-trainer">
                        <div class="card-trainer__img">
                            <img src="{{asset($trainer->image->url ?? 'images/logo-big.svg')}}">
                        </div>
                        <div class="card-trainer__text">
                            <div class="card-trainer__name">{{$trainer->first_name}} {{$trainer->last_name}}</div>
                            <div class="card-trainer__position">{{$trainer->position}}</div>
                            <div class="card-trainer__description">
                                {!! $trainer->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
