<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAdminAccess;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', CheckAdminAccess::class]);
    }

    public function index(Request $r)
    {
        $result = Product::requested($r)->get();
        return ['response' => $result];
    }

    public function store(Request $r)
    {
        $this->validate($r, [
            'name' => 'string|required|max:255',
            'description' => 'required',
            'price' => 'numeric',
            'image_id' => 'sometimes|exists:attachments,id',
        ]);

        $result = Product::create($r->all());
        return ['response' => $result];
    }

    public function show(Request $r, Product $product)
    {
        $product->loadRequested($r);
        return ['response' => $product];
    }

    public function destroy(Request $r, Product $product)
    {
        $product->delete();
        return ['response' => 1];
    }

    public function update(Request $r, Product $product)
    {
        $product->update($r->only($product->getFillable()));
        return ['response' => $product];
    }
}
