@extends('layouts.main', ['bodyClass'=>'p-index'])

@push('head')
    <title>Контакты</title>
@endpush

@section('content')
    <div class="container pb-7">
        @include('components.breadcrumbs',['items'=> [
        ['name'=>'Главная','url'=>'/'],
        ['name'=>'Контакты','url'=>route('contacts')],
        ]])
        <h1 class="h2">Контакты</h1>
        <div class="mb-5">
            @include('components.map')
        </div>
        <ul class="contacts-group contacts-group__dark mb-5">
            <li>
                <i class="material-icons">
                    location_on
                </i>
                г. Сургут, ул. 30 лет Победы, д. 66
            </li>
            <li>
                <i class="material-icons">
                    phone
                </i>
                <a class="pr-0"
                   href="tel:93-55-50">93-55-50</a> / <a class="pl-0"
                                                         href="tel:93-55-59">93-55-59</a>
            </li>
            <li>
                <i class="material-icons">
                    alternate_email
                </i>
                <a href="mailto:evolution.fit.18@inbox.ru">evolution.fit.18@inbox.ru</a>
            </li>
        </ul>
        <button class="btn btn_primary"
                data-toggle="modal" data-target="#callback-modal">Заказать обратный звонок</button>
    </div>
@endsection

@section('footer')
@endsection