<div class="schedule-card">
    <div class="schedule-card__top">
        @if($schedule->training->image)
            <img class="schedule-card__img" src="{{asset($schedule->training->image->thumbnail)}}">
        @else
            <div class="schedule-card__img bg-logo"></div>
        @endif
        <div class="schedule-card__info">
            <div class="schedule-card__time">
                {{$schedule->open_time_formatted}}
                <span class="small font-weight-bold"
                      title="Время окончания">{{$schedule->close_time_formatted}}</span>
            </div>
            <div class="mb-1">
                <div class="schedule-card__location">{{$schedule->location}}</div>
                <div class="schedule-card__class">{{$schedule->training->name}}</div>
            </div>
            <div class="schedule-card__trainer">{{$schedule->trainer}}</div>
        </div>
    </div>
    <div class="schedule-card__description">
        <div class="mb-3">
            {!! $schedule->training->description !!}
        </div>
        <button class="btn btn_dark" type="button"
                data-toggle="modal" data-target="#callback-modal">Хочу посетить!
        </button>
    </div>
</div>
