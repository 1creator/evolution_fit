import Swiper from "swiper/dist/js/swiper";
import Chocolat from 'chocolat';

export const initSwipers = function () {
    let swipers = document.getElementsByClassName("swiper-container");
    for (let i = 0; i < swipers.length; i++) {
        let item = swipers[i];
        let spaceBetween = item.getAttribute("data-spaceBetween") || 0;
        let preset = item.getAttribute("data-preset");

        switch (preset) {
            case "1":
                let initialSlideNode = item.querySelector(".date-slider-item.active").parentNode;
                let initialSlide = [...initialSlideNode.parentNode.children].indexOf(initialSlideNode);
                new Swiper(item, {
                    direction: "horizontal",
                    slidesPerView: "auto",
                    freeMode: true,
                    mousewheel: true,
                    releaseOnEdges: false,
                    initialSlide: initialSlide,
                });
                break;
            case "gallery":
                new Swiper(item, {
                    slidesPerView: 4,
                    spaceBetween: 30,
                    breakpoints: {
                        // when window width is >= 320px
                        321: {
                            slidesPerView: 2,
                        },
                        // when window width is >= 640px
                        640: {
                            slidesPerView: 3,
                        },
                        // when window width is >= 640px
                        768: {
                            spaceBetween: 10
                        },
                        992: {
                            spaceBetween: 20,
                            slidesPerView: 4,
                        }
                    },
                    pagination: {
                        el: ".swiper-pagination-progress",
                        type: "progressbar",
                    },
                    navigation: {
                        nextEl: ".progress-gallery__button-next",
                        prevEl: ".progress-gallery__button-prev",
                    },
                });

                let galleryImages = document.querySelectorAll(".progress-gallery__slide-img-wrap");
                Chocolat(galleryImages,{
                    linkImages: true,
                    allowZoom: true,
                })
                break
            default:
                new Swiper(item, {
                    direction: "horizontal",
                    autoplay: true,
                    delay: 10000,
                    spaceBetween: spaceBetween,
                    loop: true,
                    pagination: {
                        el: ".swiper-pagination",
                        clickable: true,
                        type: "bullets"
                    },
                    navigation: {
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev",
                    },
                    observer: true,
                });
                break;
        }
    }
};
