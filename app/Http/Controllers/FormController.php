<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers;


use App\Models\Schedule;
use App\Models\Trainer;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{

    private $adminEmail = 'evolution.fit.18@inbox.ru';

    public function callback(Request $r)
    {
        Mail::send('mail.callback', ['name' => $r->name, 'phone' => $r->phone], function ($message) {
            $message->to($this->adminEmail, 'Evolution Fit')->subject('Новая заявка на обратный звонок!');
        });

        return response(1);
    }

    public function trial(Request $r)
    {
        Mail::send('mail.trial', [
            'lastName' => $r->last_name,
            'firstName' => $r->first_name,
            'birthday' => $r->birthday,
            'phone' => $r->phone,
        ], function ($message) {
            $message->to($this->adminEmail, 'Evolution Fit')->subject('Новая заявка на пробное занятие!');
        });

        return response(1);
    }

    public function freezing(Request $r)
    {
        Mail::send('mail.freezing', [
            'lastName' => $r->last_name,
            'firstName' => $r->first_name,
            'birthday' => $r->birthday,
            'phone' => $r->phone,
            'from_date' => $r->from_date,
            'to_date' => $r->to_date,
        ], function ($message) {
            $message->to($this->adminEmail, 'Evolution Fit')->subject('Новая заявка на заморозку абонемента!');
        });

        return response(1);
    }
}