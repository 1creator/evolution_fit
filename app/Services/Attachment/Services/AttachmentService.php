<?php


namespace App\Services\Attachment\Services;


use App\Services\Attachment\Interfaces\AttachmentInterface;
use App\Services\Attachment\Models\Attachment;
use App\Services\Attachment\Resources\AttachmentResource;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Imagick;
use Spatie\ImageOptimizer\OptimizerChain;

class AttachmentService implements AttachmentInterface
{

    public function store($file, $type)
    {
        $createdFile = null;
        if ($type == 'embed')
            $createdFile = $this->storeIframe($file);
        else {
            $createdFile = $this->storeFile($file);
        }
        return new AttachmentResource($createdFile);
    }

    public function storeFile($file)
    {
        $path = 'upload';
        $disk = config('attachments.disk', 'public');
        app(OptimizerChain::class)->optimize($file);
        $storedFile = $file->store($path, $disk);

        try {
            $this->limitSize($storedFile, $disk);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        try {
            $thumbnail = $this->makeThumbnail($storedFile, $disk);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        $name = method_exists($file, 'getClientOriginalName') ?
            $file->getClientOriginalName() : $file->getFilename();

        return Attachment::create([
            'name' => $name,
            'type' => $file->getMimeType(),
            'size' => $file->getSize(),
            'original' => $storedFile,
            'disk' => $disk,
            'thumbnail' => $thumbnail ?? null,
        ]);
    }

    public function storeIframe($file)
    {
        return Attachment::create([
            'name' => 'Внешнее содержимое',
            'type' => 'embed',
            'original' => $file,
            'thumbnail' => null,
        ]);
    }

    /*
	   Ограничивает размер картинки до FullHD
	*/
    private function limitSize($file, $disk)
    {
        $file = Storage::disk($disk)->path($file);

        $maxWidth = 1920;
        $maxHeight = 1080;

        $im = new Imagick();
        $im->readImage($file);
        if ($im->getImageWidth() > $maxWidth || $im->getImageHeight() > $maxHeight) {
            $im->thumbnailImage(1920, 1080, true);
            $im->writeImage();
        }
    }

    /*
	   Генерация preview 300x300
	*/
    private function makeThumbnail($file, $disk)
    {
        $file = Storage::disk($disk)->path($file);
        $thumbnailDiskRelativePath = File::name($file) . '_thumbnail.png';
        $thumbnail = Storage::disk($disk)->path($thumbnailDiskRelativePath);

        $im = new Imagick();
        $im->readImage($file);
        $im->setImageFormat('png');
        $im->thumbnailImage(300, 300, true);
        $im->writeImage($thumbnail);
        return File::exists($thumbnail) ? $thumbnailDiskRelativePath : null;
    }
}
