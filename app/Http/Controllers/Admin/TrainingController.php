<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAdminAccess;
use App\Models\Training;
use Illuminate\Http\Request;

class TrainingController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', CheckAdminAccess::class]);
    }

    public function index(Request $r)
    {
        $result = Training::requested($r)->get();
        return ['response' => $result];
    }

    public function store(Request $r)
    {
        $this->validate($r, [
            'name' => 'string|required|max:255',
            'description' => 'required',
            'image_id' => 'sometimes|nullable|exists:attachments,id',
        ]);

        $result = Training::create($r->all());
        foreach ($r->schedules as $schedule){
            $result->schedules()->create($schedule);
        }
        return ['response' => $result];
    }

    public function show(Request $r, Training $training)
    {
        $training->loadRequested($r);
        return ['response' => $training];
    }

    public function destroy(Request $r, Training $training)
    {
        $training->schedules()->delete();
        $training->delete();
        return ['response' => 1];
    }

    public function update(Request $r, Training $training)
    {
        $training->update($r->only($training->getFillable()));

        $training->schedules()->delete();

        foreach ($r->schedules as $schedule){
            $training->schedules()->create($schedule);
        }

        return ['response' => $training];
    }
}
