<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAdminAccess;
use App\Models\Promo;
use Illuminate\Http\Request;

class PromoController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', CheckAdminAccess::class]);
    }

    public function index(Request $r)
    {
        $result = Promo::requested($r)->get();
        return ['response' => $result];
    }

    public function store(Request $r)
    {
        $this->validate($r, [
            'name' => 'string|required|max:255',
            'description' => 'required',
            'disabled' => 'boolean',
            'image_id' => 'required|exists:attachments,id',
        ]);

        $result = Promo::create($r->all());
        return ['response' => $result];
    }

    public function show(Request $r, Promo $promo)
    {
        $promo->loadRequested($r);
        return ['response' => $promo];
    }

    public function destroy(Request $r, Promo $promo)
    {
        $promo->delete();
        return ['response' => $promo];
    }

    public function update(Request $r, Promo $promo)
    {
        $promo->update($r->only($promo->getFillable()));
        return ['response' => $promo];
    }
}
