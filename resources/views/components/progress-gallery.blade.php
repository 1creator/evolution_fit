<!-- Swiper -->
<div class="swiper-container progress-gallery" data-preset="gallery">
    <div class="swiper-wrapper">
        @foreach($galleryItems->chunk(2) as $chunk)
            <div class="swiper-slide progress-gallery__slide">
                @foreach($chunk as $item)
                    <a class="chocolat-image progress-gallery__slide-img-wrap"
                       href="{{ $item->image->url }}"
                       title="{{ $item->title }}"
                    >
                        <img class="progress-gallery__slide-img"
                             src="{{ $item->image->url }}"
                             alt="{{ $item->title }}">
                    </a>
                @endforeach
            </div>
        @endforeach
    </div>
</div>
<div class="progress-gallery__nav">
    <div class="swiper-pagination swiper-pagination-progress"></div>
    <div class="progress-gallery__button-prev">
        <svg width="23" height="46" viewBox="0 0 23 46" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <path d="M22 1L2 23L22 45" stroke-width="2"/>
        </svg>
    </div>
    <div class="progress-gallery__button-next">
        <svg width="23" height="46" viewBox="0 0 23 46" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <path d="M1 45L21 23L1 0.999999" stroke-width="2"/>
        </svg>
    </div>
</div>
