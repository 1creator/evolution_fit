<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Utils\RequestedScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class Schedule extends RequestedScope
{
    protected $fillable = ['training_id', 'location', 'trainer_id', 'trainer', 'date',
        'day', 'open_time', 'close_time'];

    public function training()
    {
        return $this->belongsTo(Training::class);
    }

    public function trainer()
    {
        return $this->belongsTo(Training::class);
    }

    public function getOpenTimeFormattedAttribute()
    {
        return Carbon::parse($this->open_time)->tz('Asia/Yekaterinburg')->format('H:i');
    }

    public function getCloseTimeFormattedAttribute()
    {
        return Carbon::parse($this->close_time)->tz('Asia/Yekaterinburg')->format('H:i');
    }

    public static function schedulesForDate($date, $group = false)
    {
        //todo refactor

        if ($group) {
            $group = Group::findOrFail($group);
        }

        $morning = Schedule::query()->where(
            function ($query) use ($date) {
                $query->whereDate('date', $date)
                    ->orWhere('day', $date->dayOfWeekIso);
            })
            ->whereBetween('open_time', ['00:00', '7:59'])
            ->orderBy('open_time')
            ->whereHas('training.groups')
            ->when($group, function (Builder $q) use ($group) {
                $q->whereHas('training', function ($q) use ($group) {
                    $q->whereIn('id', $group->trainings->pluck('id'));
                });
            });


        $day = Schedule::where(
            function ($query) use ($date) {
                $query->whereDate('date', $date)
                    ->orWhere('day', $date->dayOfWeekIso);
            })
            ->whereBetween('open_time', ['7:00', '12:59'])
            ->orderBy('open_time')
            ->whereHas('training.groups')
            ->when($group, function (Builder $q) use ($group) {
                $q->whereHas('training', function ($q) use ($group) {
                    $q->whereIn('id', $group->trainings->pluck('id'));
                });
            });

        $evening = Schedule::where(
            function ($query) use ($date) {
                $query->whereDate('date', $date)
                    ->orWhere('day', $date->dayOfWeekIso);
            })
            ->whereBetween('open_time', ['13:00', '23:59'])
            ->orderBy('open_time')
            ->whereHas('training.groups')
            ->when($group, function (Builder $q) use ($group) {
                $q->whereHas('training', function ($q) use ($group) {
                    $q->whereIn('id', $group->trainings->pluck('id'));
                });
            });

        return [
            'morning' => $morning->get(),
            'day' => $day->get(),
            'evening' => $evening->get(),
        ];
    }
}
