<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 27.12.2018
 * Time: 23:19
 */

namespace App\Utils;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class RequestedScope extends Model
{
    protected $able_with = [];

    public function scopeRequested(Builder $query, Request $request)
    {
        if (is_array($request->with)) {
            foreach ($request->with as $item) {
                if (!in_array($item, $this->able_with)) {
                    continue;
                }

                $method = 'with' . ucfirst($item);

                if (method_exists($this, $method)) {
                    $this->$method($query);
                } else {
                    $query->with($item);
                }
            }
        }

        if (is_array($request->with_count)) {
            foreach ($request->with_count as $item) {
                $query->withCount($item);
            }
        }

        if ($request->offset || $request->limit) {
            $query->offset($request->offset ? $request->offset : 0);
            $query->limit($request->limit ? $request->limit : 10);
        }

        if ($request->order_by) {
            $arr = explode(":", $request->order_by);

            $method = 'orderBy' . ucfirst($arr[0]);

            if (method_exists($this, $method)) {
                $this->$method($query);
            } else {
                $query->orderBy($arr[0], $arr[1] ?? 'asc');
            }
        }

        if ($request->filters) {
            foreach ($request->filters as $params) {
                $params = json_decode($params);

                $method = 'filter' . ucfirst($params[0]);

                if (method_exists($this, $method)) {
                    $this->$method($query);
                } else {
                    $query->where($params[0], $params[1], $params[2], $params[3] ?? 'and');
                }
            }
        }

        return $query;
    }

    public function loadRequested(Request $request)
    {
        if ($request->with) {
            $items = [];
            foreach ($request->with as $item) {
                if (!in_array($item, $this->able_with)) {
                    continue;
                }

                $method = 'with' . ucfirst($item);

                if (method_exists($this, $method)) {
                    //todo
                } else {
                    array_push($items, $item);
                }
            }
            $this->load($items);
        }

        if ($request->with_count) {
            foreach ($request->with_count as $item) {
                $this->loadCount($item);
            }
        }

        return $this;
    }
}
