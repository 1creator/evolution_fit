<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Utils\RequestedScope;

class Group extends RequestedScope
{
    protected $fillable = ['name'];
    protected $able_with = ['trainings'];

    public function trainings()
    {
        return $this->belongsToMany(Training::class, 'group_training');
    }
}