<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'SiteController@index')->name('index');
Route::get('/schedule', 'SiteController@schedule')->name('schedule');
Route::get('/promos', 'SiteController@promos')->name('promos');
Route::get('/cards', 'CardController@index')->name('cards');
Route::get('/cards/freezing', 'CardController@freezing')->name('cards.freezing');
Route::get('/cards/trial', 'CardController@trial')->name('cards.trial');
Route::get('/trainers', 'SiteController@trainers')->name('trainers');
Route::get('/contacts', 'SiteController@contacts')->name('contacts');

Route::post('/callback', 'FormController@callback')->name('callback');
Route::post('/cards/trial', 'FormController@trial')->name('cards.trial');
Route::post('/cards/freezing', 'FormController@freezing')->name('cards.freezing');

require_once 'admin.php';

Route::post('/ajax/files', 'Ajax\FileController@upload');