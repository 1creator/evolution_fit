<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAttachmentForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
           $table->dropForeign('products_image_id_foreign');
            $table->foreign('image_id')->references('id')->on('attachments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('trainings', function (Blueprint $table) {
           $table->dropForeign('trainings_image_id_foreign');
            $table->foreign('image_id')->references('id')->on('attachments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('trainers', function (Blueprint $table) {
            $table->dropForeign('trainers_image_id_foreign');
            $table->foreign('image_id')->references('id')->on('attachments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('promos', function (Blueprint $table) {
           $table->dropForeign('promos_image_id_foreign');
            $table->foreign('image_id')->references('id')->on('attachments')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

}
