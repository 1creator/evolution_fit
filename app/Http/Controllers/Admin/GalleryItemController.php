<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAdminAccess;
use App\Models\GalleryItem;
use App\Services\Attachment\Models\Attachment;
use Illuminate\Http\Request;

class GalleryItemController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', CheckAdminAccess::class]);
    }

    public function index(Request $r)
    {
        $result = GalleryItem::requested($r)->get();
        return ['response' => $result];
    }

    public function store(Request $r)
    {
        $this->validate($r, [
            'name' => 'string|required|max:255',
        ]);

        $result = GalleryItem::create($r->all());
        return ['response' => $result];
    }

    public function show(Request $r, GalleryItem $galleryItem)
    {
        $galleryItem->loadRequested($r);
        return ['response' => $galleryItem];
    }

    public function destroy(Request $r, GalleryItem $galleryItem)
    {
        $galleryItem->delete();
        return ['response' => 1];
    }

    public function update(Request $r, GalleryItem $galleryItem)
    {
        $galleryItem->update($r->only(['title']));
        return ['response' => $galleryItem];
    }

    public function sync(Request $request)
    {
        $items = collect();
        foreach ($request->items as $item) {
            /** @var GalleryItem $galleryItem */
            $galleryItem = GalleryItem::updateOrCreate([
                'id' => $item['id'] ?? null
            ], [
                'title' => $item['title'],
            ]);
            $galleryItem->image()->save(Attachment::findOrFail($item['image_id']));

            $items->add($galleryItem);
        }
        GalleryItem::whereNotIn('id', $items->pluck('id'))->delete();

        return GalleryItem::all();
    }
}
