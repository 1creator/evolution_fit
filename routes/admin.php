<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 25.08.2019
 * Time: 1:35
 */


use App\Http\Middleware\CheckAdminAccess;
use Illuminate\Support\Facades\Route;

Route::redirect('/admin', '/admin/trainings');

Route::get('/admin/{any}', function () {
    return view('layouts.admin');
})->where('any', '.*')->middleware(['auth', CheckAdminAccess::class]);

Route::group([
    'prefix' => 'ajax/admin',
    'namespace' => 'Admin',
], function () {
    Route::put('gallery-items/sync', 'GalleryItemController@sync');

    Route::apiResource('/promos', 'PromoController');
    Route::apiResource('/trainers', 'TrainerController');
    Route::apiResource('/products', 'ProductController');
    Route::apiResource('/trainings', 'TrainingController');
    Route::apiResource('/groups', 'GroupController');
    Route::apiResource('/gallery-items', 'GalleryItemController');
});

