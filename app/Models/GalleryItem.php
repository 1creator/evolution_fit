<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Services\Attachment\Models\Attachment;
use App\Utils\RequestedScope;

class GalleryItem extends RequestedScope
{
    protected $fillable = ['title'];
    protected $with = ['image'];
    public $timestamps = null;

    public function image()
    {
        return $this->morphOne(Attachment::class, 'attachable');
    }
}
