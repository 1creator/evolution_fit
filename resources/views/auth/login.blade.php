@extends('layouts.main', ['bodyClass'=>'p-index'])

@push('head')
    <title>Авторизация</title>
@endpush

@section('content')
    <div class="container">
        <h1 class="h2">Авторизация</h1>
        <div class="row mb-5">
            <div class="col-12 col-sm-6 col-md-4">
                <form action="" method="post">
                    {{csrf_field()}}
                    <div class="input">
                        <label for="email">Email</label>
                        <input type="email" id="email" name="email">
                    </div>
                    <div class="input">
                        <label for="password">Пароль</label>
                        <input type="password" id="password" name="password">
                    </div>
                    @if($errors->count()>0)
                        <div class="message message_danger my-3">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
                    <button class="btn btn_dark">Войти</button>
                </form>
            </div>
        </div>
    </div>
@endsection